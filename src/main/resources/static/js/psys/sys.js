var PSys = {};
PSys.isEmpty = function (o) {
	return o == undefined || o == null || o == '';
};

// { pageOffset : 0, selectedIds:'1,2', module : 'SysDept', url:'/abc/efg'}
PSys.loadList = function (options) {
	var opts = Object.assign({
		pageOffset : 0
	}, options);
	// 参数检查
	if (PSys.isEmpty(opts.module)) {
		console.log('参数module未设置！');
		return;
	}
	if (PSys.isEmpty(opts.url)) {
		console.log('url未设置！');
		return;
	}
	// 获取表单中所有key-value，包含查询字段、列表id的字段、分页字段
	var t = $('#form' + opts.module + 'List').serializeArray();
	var d = {};
	$.each(t, function(i, field) {
		if (d[field.name]) {
			d[field.name] = d[field.name] + "," + field.value;
		} else {
			d[field.name] = field.value;
		}
	});
	if (d['page'] == undefined) {
		d['page'] = 1;
	}
	d['page'] = parseInt(d['page']) + opts.pageOffset;
	// 加载列表数据，成功后，比对selectedIds和列表中的id值，设置checkbox选中状态
	$('#div' + opts.module + 'List').load(opts.url, d, function() {
		if (opts.selectedIds) {
			var ids = opts.selectedIds.split(',');
			$('input[type="checkbox"][name="id"]').each(function() {
				// this：input节点，html5的element
				// $(this)：input节点的jquery对象
				// [{},{}]
				// {a:,b:}
				for ( var i in ids) {
					if (ids[i] == this.value) {
						$(this).prop('checked', true);
					}
				}
			});
		}
		// 重新应用拼图的js事件
		pintuer();
	});
};

// “批量移除”按钮的响应
// 参数说明：{ module : 'SysDept', url:'/abc/efg', fn : function () {}}
// fn参数可选
PSys.deleteBatch = function (options) {
	var opts = Object.assign({}, options);
	// 参数检查
	if (PSys.isEmpty(opts.module)) {
		console.log('参数module未设置！');
		return;
	}
	if (PSys.isEmpty(opts.url)) {
		console.log('url未设置！');
		return;
	}
	// 至少选择一条记录删除
	var input = $(".table input:checkbox:checked");
	if (input.length == 0) {
		alert('请选择至少一条记录！');
		return;
	}
	// 弹出确认框让用户确认删除
	if (!confirm('该操作不可恢复，确定要删除选中的记录吗？')) {
		return;
	}
	// 获取表单参数
	var t = $('#form' + opts.module + 'List').serializeArray();
	// 提交删除请求
	$.post(opts.url, t, function(result) {
		if (result.success) {
			if (opts.fn) {
				opts.fn();
			}
		}
		alert(result.message);
	});
}

PSys.validateForm = function(domSel) {
	if (!domSel) {
		domSel = 'form';
	}
	var $hideplaceholder = function(element) {
		if ($(element).data("pintuerholder")) {
			$(element).val("");
			$(element).css("color", $(element).data("pintuerholder"));
			$(element).removeData("pintuerholder");
		}
	};
	$(domSel)
			.find(
					'input[data-validate],textarea[data-validate],select[data-validate]')
			.trigger("blur");
	$(domSel).find('input[placeholder],textarea[placeholder]').each(function() {
		$hideplaceholder(this);
	});
	var numError = $(domSel).find('.check-error').length;
	if (numError) {
		return false;
	}
	return true;
};

PSys.addAdminDiv = function (bread) {
	$('<li><a class="icon-arrow-down"> ' + bread + '</a></li>').appendTo ('.bread');
	return $("<div class='admin' />").appendTo($("body"));
};

PSys.removeTopAdminDiv = function () {
	// 找到最后一个class是admin的div并删除
	$('.admin:last').remove ();
	$('.bread li:last').remove ();
};

PSys.getAdminDivLength = function () {
	return $('.admin').length;
};

// 多选选择器某行的checkbox点击时的事件响应
// opts = {id : '', name : '', checked : true, module : 'SysRegionSelector'}
PSys.selectMultipleSelector = function (opts) {
	var ids = $("#form" + opts.module + "List input[name='selectedIds']").val();
	var names = $("#form" + opts.module + "List input[name='selectedNames']").val();
	if (!ids) {
		ids = [];
		names = [];
	} else {
		ids = ids.split (',');
		names = names.split (',');
	}
	if (opts.checked) {// 如果角色选中
		ids.push (opts.id);
		names.push (opts.name);
	} else {// 如果取消选中，则找到对应的id，并删除
		for (var i in ids) {
			if (ids[i] == opts.id) {
				ids.splice(i, 1);
				names.splice(i, 1);
				break;
			}
		}
	}
	console.log (ids);
	console.log (names);
	$("#form" + opts.module + "List input[name='selectedIds']").val(ids.join (','));
	$("#form" + opts.module + "List input[name='selectedNames']").val(names.join (','));
};

// 单选选择器某行的checkbox点击时的事件响应
//opts = {id : '', name : '', checked : true, module : 'SysRegionSelector'}
PSys.selectSingleSelector = function (opts) {
	if (opts.checked) {
		$("#form" + opts.module + "List input[name='selectedIds']").val(opts.id);
		$("#form" + opts.module + "List input[name='selectedNames']").val(opts.name);
		// 清除所有行的checkbox选中
		$("#form" + opts.module + "List input[type='checkbox']").prop ('checked', false);
		// 设置当前点击行checkbox为选中
		$("#form" + opts.module + "List input[type='checkbox'][value='" + opts.id + "']").prop ('checked', true);
	} else {
		$("#form" + opts.module + "List input[name='selectedIds']").removeAttr('value');
		$("#form" + opts.module + "List input[name='selectedNames']").removeAttr('value');
	}
}

// 选择器列表中确定按钮点击事件响应
PSys.selectSelectorOK = function (module) {
	var id = $("#form" + module + "List input[name='selectedIds']").val();
	var name = $("#form" + module + "List input[name='selectedNames']").val();
	var callback = $("#form" + module + "List input[name='callback']").val();
	console.log (callback + "('" + id + "', '" + name + "')");
	var result = eval (callback + "('" + id + "', '" + name + "')");
	if (result == true) {
		PSys.removeTopAdminDiv ();
	}
}

// 加载表单
PSys.loadForm = function (options) {
	var opts = Object.assign({
		title : '表单',
		url : ''
	}, options);
	PSys.addAdminDiv(opts.title).load (opts.url, function () {
		// 重新应用拼图的js事件
		pintuer();
	});
};

// 提交表单 {formSelector:'', url:'', callback : function () {}, fn : function (result) {}}
PSys.submitForm = function (options) {
	var opts = Object.assign({
		formSelector : 'form',
		url : ''
	}, options);
	if (!PSys.validateForm (opts.formSelector)) {
		return;
	}
	$.post(opts.url, $(opts.formSelector).serializeArray(), function(result) {
		if (opts.fn) {
			opts.fn (result);
			return;
		}
		if (result.success) {
			alert(result.message);
			PSys.removeTopAdminDiv();
			if (opts.callback) {
				opts.callback ();
			}
		} else {
			alert(result.message);
		}
	});
}