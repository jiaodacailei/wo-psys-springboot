package com.qfedu.psys.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.qfedu.psys.po.User;

/**
 * 实体User基于spring-data的DAO接口.
 * @author cailei
 */
public interface UserRepository extends PagingAndSortingRepository<User, java.lang.String>{
	
	Page<User> findAllByLoginNameLike(java.lang.String searchContent, Pageable pageInput);

	User findByLoginName(String user);
}
