package com.qfedu.psys.repository;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.qfedu.psys.po.Menu;

/**
 * 实体Menu基于spring-data的DAO接口.
 * @author cailei
 */
public interface MenuRepository extends PagingAndSortingRepository<Menu, java.lang.String>{
	
	Page<Menu> findAllByNameLike(java.lang.String searchContent, Pageable pageInput);

	List<Menu> findAllByParentIdIsNullOrderByNo();

	@Query ("select r.menus from Role r where r.id in (:roleIds)")
	List<Menu> findAllByRoleIds (@Param("roleIds")List<String> roleIds);
}
