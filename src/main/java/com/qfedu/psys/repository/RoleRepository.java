package com.qfedu.psys.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.qfedu.psys.po.Role;

/**
 * 实体Role基于spring-data的DAO接口.
 * @author cailei
 */
public interface RoleRepository extends PagingAndSortingRepository<Role, java.lang.String>{
	
	Page<Role> findAllByNameLike(java.lang.String searchContent, Pageable pageInput);
}
