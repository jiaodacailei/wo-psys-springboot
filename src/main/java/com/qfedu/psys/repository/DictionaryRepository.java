package com.qfedu.psys.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.qfedu.psys.po.Dictionary;

/**
 * 实体Dictionary基于spring-data的DAO接口.
 * @author cailei
 */
public interface DictionaryRepository extends PagingAndSortingRepository<Dictionary, java.lang.String>{
	
	Page<Dictionary> findAllByDicTypeLike(java.lang.String searchContent, Pageable pageInput);
}
