package com.qfedu.psys.dto;

import java.io.Serializable;

import com.qfedu.common.util.WoUtil;
import com.qfedu.psys.po.User;

/**
 * 放在session中的用户数据类.
 * @author cailei
 * @date Nov 3, 2018
 */
public class WoUser extends UserDto_ implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 8498380110342879997L;
    
    private String currentRoleId = "";
    
    private String currentRoleName = "";
    
    private String staffId = "";

    private String staffName = "";
    
    private String regionId = "";

    private String regionName = "";
    
    private String positionIds;
    
    private String positionNames;
    
    /**
     * 无参构造函数
     */
    public WoUser() {
    }

    /**
	 * 构造函数,通过po构造dto
	 */
	public WoUser(User po) {
		super (po);
	}
	
    public String getCurrentRoleId() {
		return currentRoleId;
	}

	public void setCurrentRoleId(String currentRoleId) {
		this.currentRoleId = currentRoleId;
		String[] ids = WoUtil.splitIds(getRolesId());
		String[] names = WoUtil.splitIds(getRolesName());
		for (int i = 0; i < ids.length; i ++) {
			if (ids[i].equals(currentRoleId)) {
				this.currentRoleName = names[i];
			}
		}
	}

	public String getCurrentRoleName() {
		return currentRoleName;
	}

	public void setCurrentRoleName(String currentRoleName) {
		this.currentRoleName = currentRoleName;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getPositionIds() {
		return positionIds;
	}

	public void setPositionIds(String positionIds) {
		this.positionIds = positionIds;
	}

	public String getPositionNames() {
		return positionNames;
	}

	public void setPositionNames(String positionNames) {
		this.positionNames = positionNames;
	}

	/**
     * @return
     */
    public boolean isAdmin() {
    	for (String r : WoUtil.splitIds(getRolesId())) {
    		if ("admin".equals(r)) {
    			return true;
    		}
    	}
        return false;
    }
}
