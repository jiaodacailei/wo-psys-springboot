package com.qfedu.psys.dto;

import java.util.ArrayList;
import java.util.List;

import com.qfedu.common.entity.WoPage;
import com.qfedu.psys.po.User;

/**
 * PO实体User对应的DTO类.
 * @author cailei
 */
public class UserDto extends UserDto_ {

	/**
	 * 无参构造函数
	 */
	public UserDto() {
		super ();
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public UserDto(User po) {
		super (po);
	}
	
	/**
	 * 将po列表数据转化为dto列表数据
	 * @param pos
	 * @return
	 */
	public static List<UserDto> getDtos (List<User> pos) {
		List<UserDto> rs = new ArrayList<UserDto>();
		for (User r : pos) {
			UserDto dto = new UserDto(r);
			rs.add(dto);
		}
		return rs;
	}
	
	/**
	 * 将分页po数据转化为dto分页数据
	 * @param pos
	 * @param total
	 * @return
	 */
	public static WoPage<UserDto> getPageData(List<User> pos, Long total) {
		WoPage<UserDto> puDto = new WoPage<UserDto>(getDtos(pos), total);
		return puDto;
	}
}
