package com.qfedu.psys.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import com.qfedu.psys.po.Menu;

/**
 * PO实体Menu对应的DTO类.
 * @author cailei
 */
public class MenuDto extends MenuDto_ {

	/**
	 * 是否当前菜单
	 */
	private Boolean current = false;
	
	/**
	 * 业务ID
	 */
	private String bizId;
	
	/**
	 * 无参构造函数
	 */
	public MenuDto() {
		super ();
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public MenuDto(Menu po) {
		super (po);
	}
	
	public Boolean getCurrent() {
		return current;
	}

	public void setCurrent(Boolean current) {
		this.current = current;
	}
	public String getBizId() {
		return bizId;
	}

	public Boolean hasBizId () {
		return !WoUtil.isEmpty(bizId);
	}
	
	public void setBizId(String bizId) {
		this.bizId = bizId;
	}
	
	/**
	 * 获取第一个子菜单的url
	 * @return
	 */
	public String getFirstChildUrl () {
		if (getChildren().size() > 0) {
			return getChildren().get(0).getUrl();
		}
		return null;
	}
	
	/**
	 * @param menus
	 */
	public static void sort (List<Menu> menus) {
		Collections.sort(menus, new Comparator<Menu>() {

			@Override
			public int compare(Menu o1, Menu o2) {
				String n1 = o1.getNo() == null ? "" : o1.getNo();
				String n2 = o2.getNo() == null ? "" : o2.getNo();
				return n1.compareToIgnoreCase(n2);
			}
		});
	}
	
	/**
	 * 将po列表数据转化为dto列表数据
	 * @param pos
	 * @return
	 */
	public static List<MenuDto> getDtos (List<Menu> pos) {
		List<MenuDto> rs = new ArrayList<MenuDto>();
		for (Menu r : pos) {
			MenuDto dto = new MenuDto(r);
			rs.add(dto);
		}
		return rs;
	}
	
	/**
	 * 将分页po数据转化为dto分页数据
	 * @param pos
	 * @param total
	 * @return
	 */
	public static WoPage<MenuDto> getPageData(List<Menu> pos, Long total) {
		WoPage<MenuDto> puDto = new WoPage<MenuDto>(getDtos(pos), total);
		return puDto;
	}
}
