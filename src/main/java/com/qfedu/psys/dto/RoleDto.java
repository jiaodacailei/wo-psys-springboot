package com.qfedu.psys.dto;

import java.util.ArrayList;
import java.util.List;

import com.qfedu.common.entity.WoPage;
import com.qfedu.psys.po.Role;

/**
 * PO实体Role对应的DTO类.
 * @author cailei
 */
public class RoleDto extends RoleDto_ {

	/**
	 * 无参构造函数
	 */
	public RoleDto() {
		super ();
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public RoleDto(Role po) {
		super (po);
	}
	
	/**
	 * 将po列表数据转化为dto列表数据
	 * @param pos
	 * @return
	 */
	public static List<RoleDto> getDtos (List<Role> pos) {
		List<RoleDto> rs = new ArrayList<RoleDto>();
		for (Role r : pos) {
			RoleDto dto = new RoleDto(r);
			rs.add(dto);
		}
		return rs;
	}
	
	/**
	 * 将分页po数据转化为dto分页数据
	 * @param pos
	 * @param total
	 * @return
	 */
	public static WoPage<RoleDto> getPageData(List<Role> pos, Long total) {
		WoPage<RoleDto> puDto = new WoPage<RoleDto>(getDtos(pos), total);
		return puDto;
	}
}
