package com.qfedu.psys.controller;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.entity.WoResultCode;
import com.qfedu.common.entity.WoSelectorParams;
import com.qfedu.common.exception.handler.WoControllerExceptionHandler;
import com.qfedu.psys.util.PSysConstant;
import com.qfedu.psys.util.PSysUtil;
import com.qfedu.psys.dto.MenuDto;
import com.qfedu.psys.service.CoreService;
import com.qfedu.psys.dto.UserDto;
import com.qfedu.psys.service.UserService;
/**
 * 用户对应控制器
 * @author cailei
 */
@Controller
@SessionAttributes(names=PSysConstant.SESSION_USER)
@RequestMapping ("/psys/user")
public class UserController {
	private final static Logger LOG = LogManager.getLogger(UserController.class);
	
	/**
	 * 注入UserService.
	 */
	@Resource
	private UserService userService;
	
	/**
	 * 注入MenuService
	 */
	@Resource
	private CoreService coreService;
	
	/**
	 * 注入异常处理器.
	 */
	@Resource
	private WoControllerExceptionHandler exceptionHandler;
	
	/**
	 * 加载整个用户管理页面.
	 * @param map
	 * @return
	 */
	@RequestMapping ("")
	public String main (Map<String, Object> map) {
		return exceptionHandler.toLoginView(() -> {
			List<MenuDto> menus = coreService.getMenus(PSysUtil.getCurrentUser(map), "menu-psys", "menu-user");
			map.put("menus", menus);
		}, map, "psys/user");
	}
	
	/**
	 * 前端加载整个用户管理页面后,会发送AJAX请求加载用户列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/list")
	public String list(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<UserDto> pageData = userService.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "psys/user-list");
	}
	
	/**
	 * 加载用户创建表单.
	 * @param map
	 * @return
	 */
	@GetMapping("/create")
	public String create(Map<String, Object> map) {
		return "psys/user-create";
	}
	
	/**
	 * 提交用户创建表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/create")
	@ResponseBody
	public WoResultCode create(UserDto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> userService.create(PSysUtil.getCurrentUser(map), dto), "创建用户成功！");
	}
	
	/**
	 * 加载用户修改表单.
	 * @param id
	 * @param map
	 * @return
	 */
	@GetMapping("/update")
	public String update(String id, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			map.put("formData", userService.getById(id));
		}, map, "psys/user-update");
	}
	
	/**
	 * 提交用户修改表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/update")
	@ResponseBody
	public WoResultCode update(UserDto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> userService.update(dto), "修改用户成功！");
	}
	
	/**
	 * 批量删除用户.
	 * @param id
	 * @param map
	 * @return
	 */
	@PostMapping("/delete")
	@ResponseBody
	public WoResultCode delete(String[] id, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> userService.delete(id), "删除用户成功！");
	}
	
	/**
	 * 加载用户选择器页面.
	 * @param params
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector")
	public String loadSelector(WoSelectorParams params, Map<String, Object> map) {
		map.put("params", params);
		return "psys/user-selector";
	}
	
	/**
	 * 前端加载整个用户选择器页面后,会发送AJAX请求加载用户选择器列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector/list")
	public String selectorList(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<UserDto> pageData = userService.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "psys/user-selector-list");
	}
}
