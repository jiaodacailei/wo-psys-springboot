package com.qfedu.psys.controller;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.entity.WoResultCode;
import com.qfedu.common.entity.WoSelectorParams;
import com.qfedu.common.exception.handler.WoControllerExceptionHandler;
import com.qfedu.psys.util.PSysConstant;
import com.qfedu.psys.util.PSysUtil;
import com.qfedu.psys.dto.MenuDto;
import com.qfedu.psys.service.CoreService;
import com.qfedu.psys.dto.RoleDto;
import com.qfedu.psys.service.RoleService;
/**
 * 角色对应控制器
 * @author cailei
 */
@Controller
@SessionAttributes(names=PSysConstant.SESSION_USER)
@RequestMapping ("/psys/role")
public class RoleController {
	private final static Logger LOG = LogManager.getLogger(RoleController.class);
	
	/**
	 * 注入RoleService.
	 */
	@Resource
	private RoleService roleService;
	
	/**
	 * 注入MenuService
	 */
	@Resource
	private CoreService coreService;
	
	/**
	 * 注入异常处理器.
	 */
	@Resource
	private WoControllerExceptionHandler exceptionHandler;
	
	/**
	 * 加载整个角色管理页面.
	 * @param map
	 * @return
	 */
	@RequestMapping ("")
	public String main (Map<String, Object> map) {
		return exceptionHandler.toLoginView(() -> {
			List<MenuDto> menus = coreService.getMenus(PSysUtil.getCurrentUser(map), "menu-psys", "menu-role");
			map.put("menus", menus);
		}, map, "psys/role");
	}
	
	/**
	 * 前端加载整个角色管理页面后,会发送AJAX请求加载角色列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/list")
	public String list(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<RoleDto> pageData = roleService.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "psys/role-list");
	}
	
	/**
	 * 加载角色创建表单.
	 * @param map
	 * @return
	 */
	@GetMapping("/create")
	public String create(Map<String, Object> map) {
		return "psys/role-create";
	}
	
	/**
	 * 提交角色创建表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/create")
	@ResponseBody
	public WoResultCode create(RoleDto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> roleService.create(PSysUtil.getCurrentUser(map), dto), "创建角色成功！");
	}
	
	/**
	 * 加载角色修改表单.
	 * @param id
	 * @param map
	 * @return
	 */
	@GetMapping("/update")
	public String update(String id, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			map.put("formData", roleService.getById(id));
		}, map, "psys/role-update");
	}
	
	/**
	 * 提交角色修改表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/update")
	@ResponseBody
	public WoResultCode update(RoleDto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> roleService.update(dto), "修改角色成功！");
	}
	
	/**
	 * 批量删除角色.
	 * @param id
	 * @param map
	 * @return
	 */
	@PostMapping("/delete")
	@ResponseBody
	public WoResultCode delete(String[] id, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> roleService.delete(id), "删除角色成功！");
	}
	
	/**
	 * 加载角色选择器页面.
	 * @param params
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector")
	public String loadSelector(WoSelectorParams params, Map<String, Object> map) {
		map.put("params", params);
		return "psys/role-selector";
	}
	
	/**
	 * 前端加载整个角色选择器页面后,会发送AJAX请求加载角色选择器列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector/list")
	public String selectorList(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<RoleDto> pageData = roleService.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "psys/role-selector-list");
	}
}
