package com.qfedu.psys.controller;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.entity.WoResultCode;
import com.qfedu.common.entity.WoSelectorParams;
import com.qfedu.common.exception.handler.WoControllerExceptionHandler;
import com.qfedu.psys.util.PSysConstant;
import com.qfedu.psys.util.PSysUtil;
import com.qfedu.psys.dto.MenuDto;
import com.qfedu.psys.service.CoreService;
import com.qfedu.psys.dto.DictionaryDto;
import com.qfedu.psys.service.DictionaryService;
/**
 * 数据字典对应控制器
 * @author cailei
 */
@Controller
@SessionAttributes(names=PSysConstant.SESSION_USER)
@RequestMapping ("/psys/dictionary")
public class DictionaryController {
	private final static Logger LOG = LogManager.getLogger(DictionaryController.class);
	
	/**
	 * 注入DictionaryService.
	 */
	@Resource
	private DictionaryService dictionaryService;
	
	/**
	 * 注入MenuService
	 */
	@Resource
	private CoreService coreService;
	
	/**
	 * 注入异常处理器.
	 */
	@Resource
	private WoControllerExceptionHandler exceptionHandler;
	
	/**
	 * 加载整个数据字典管理页面.
	 * @param map
	 * @return
	 */
	@RequestMapping ("")
	public String main (Map<String, Object> map) {
		return exceptionHandler.toLoginView(() -> {
			List<MenuDto> menus = coreService.getMenus(PSysUtil.getCurrentUser(map), "menu-psys", "menu-dictionary");
			map.put("menus", menus);
		}, map, "psys/dictionary");
	}
	
	/**
	 * 前端加载整个数据字典管理页面后,会发送AJAX请求加载数据字典列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/list")
	public String list(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<DictionaryDto> pageData = dictionaryService.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "psys/dictionary-list");
	}
	
	/**
	 * 加载数据字典创建表单.
	 * @param map
	 * @return
	 */
	@GetMapping("/create")
	public String create(Map<String, Object> map) {
		return "psys/dictionary-create";
	}
	
	/**
	 * 提交数据字典创建表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/create")
	@ResponseBody
	public WoResultCode create(DictionaryDto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> dictionaryService.create(PSysUtil.getCurrentUser(map), dto), "创建数据字典成功！");
	}
	
	/**
	 * 加载数据字典修改表单.
	 * @param id
	 * @param map
	 * @return
	 */
	@GetMapping("/update")
	public String update(String id, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			map.put("formData", dictionaryService.getById(id));
		}, map, "psys/dictionary-update");
	}
	
	/**
	 * 提交数据字典修改表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/update")
	@ResponseBody
	public WoResultCode update(DictionaryDto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> dictionaryService.update(dto), "修改数据字典成功！");
	}
	
	/**
	 * 批量删除数据字典.
	 * @param id
	 * @param map
	 * @return
	 */
	@PostMapping("/delete")
	@ResponseBody
	public WoResultCode delete(String[] id, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> dictionaryService.delete(id), "删除数据字典成功！");
	}
	
	/**
	 * 加载数据字典选择器页面.
	 * @param params
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector")
	public String loadSelector(WoSelectorParams params, Map<String, Object> map) {
		map.put("params", params);
		return "psys/dictionary-selector";
	}
	
	/**
	 * 前端加载整个数据字典选择器页面后,会发送AJAX请求加载数据字典选择器列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector/list")
	public String selectorList(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<DictionaryDto> pageData = dictionaryService.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "psys/dictionary-selector-list");
	}
}
