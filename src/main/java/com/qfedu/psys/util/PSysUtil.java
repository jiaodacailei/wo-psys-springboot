package com.qfedu.psys.util;

import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.qfedu.common.WoException;
import com.qfedu.psys.dto.WoUser;

import wo.jpa.psys.generator.PSysGenerator;

public class PSysUtil {

	private final static Logger LOG = LogManager.getLogger(PSysUtil.class);

	/**
	 * 从shiro中获取当前用户信息
	 * 
	 * @return
	 * 
	 * 		public static UserDto getCurrentUser () { return
	 *         (UserDto)SecurityUtils.getSubject().getPrincipal(); }
	 */

	/**
	 * 从session中获取当前登录用户信息，如果不存在，则抛出异常
	 * 
	 * @param map
	 * @return
	 */
	public static WoUser getCurrentUser(Map<String, Object> map) {
		WoUser u = (WoUser) map.get(PSysConstant.SESSION_USER);
		if (u == null) {
			throw new WoException(PSysConstant.ERR_LOGIN_NOT);
		}
		return u;
	}

	/**
	 * 将异常消息设置到request的attribute中
	 * @param map
	 * @param e
	 * @param unknownMsg
	 */
	public static void setExceptionMessage(Map<String, Object> map, Exception e, String unknownMsg) {
		LOG.error(e.getMessage(), e);
		if (e instanceof WoException) {
			map.put("error", e.getMessage());
		} else {
			map.put("error", unknownMsg);
		}
	}
	
	public static void main (String[] args) {
		// PSysGenerator.createCodeByPackage("./src/main/java/", "com.qfedu.psys.po");
		// PSysGenerator.createCodeByPackage("./src/main/java/", "com.qfedu.department.po");
		PSysGenerator.createHtmlByPackage("./src/main/resources/", "com.qfedu.psys.po");
		// PSysGenerator.createHtmlByPackage("./src/main/resources/", "com.qfedu.department.po");
		// PSysGenerator.createHtml("./src/main/resources/", "com.qfedu.department.po.Position", "dpt");
		// PSysGenerator.createHtml("./src/main/resources/", "com.qfedu.department.po.Department", "dpt");
		// PSysGenerator.createHtml("./src/main/resources/", "com.qfedu.department.po.Staff", "dpt");
		// PSysGenerator.createHtml("./src/main/resources/", "com.qfedu.psys.po.Menu", "sys");
	}
}
