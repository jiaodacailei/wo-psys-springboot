package com.qfedu.psys;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.qfedu.common.WoException;
import com.qfedu.common.entity.WoResultCode;

/**
 * Pintuer系统管理异常类
 * @author cailei
 *
 */
public class PSysException extends WoException {
	
	private final static Logger LOG = LogManager.getLogger(PSysException.class);
	
	public PSysException() {
	}

	public PSysException(WoResultCode code, Object... args) {
		super(code, args);
	}

	public PSysException(Throwable cause, WoResultCode code, Object... args) {
		super(cause, code, args);
	}
}
