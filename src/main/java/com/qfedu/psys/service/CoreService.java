package com.qfedu.psys.service;

import java.util.List;

import com.qfedu.psys.dto.MenuDto;
import com.qfedu.psys.dto.WoUser;

/**
 * 系统核心服务对应的Service接口.
 * @author cailei
 */
public interface CoreService {
		
	/**
	 * @param username
	 * @param password
	 * @param roleId
	 * @return
	 */
	WoUser authenticate(String username, String password, String roleId);
	
	/**
	 * @param u
	 * @param topId
	 * @param subId
	 * @return
	 */
	public List<MenuDto> getMenus(WoUser u, String topId, String subId);
}
