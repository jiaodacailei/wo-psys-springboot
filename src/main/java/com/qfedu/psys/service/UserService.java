package com.qfedu.psys.service;

import com.qfedu.common.entity.WoPage;
import com.qfedu.psys.dto.UserDto;
import com.qfedu.psys.dto.WoUser;

/**
 * PO实体User对应的Service接口.
 * @author cailei
 */
public interface UserService {
		
	/**
	 * 列表
	 * @param currentUser 当前用户
	 * @param searchContent 查询内容文本
	 * @param page 页索引,从1开始
	 * @param size 一页的最大记录数
	 * @return
	 */
	WoPage<UserDto> getPageData(WoUser currentUser, String searchContent, Long page, Long size);
	
	/**
	 * 创建
	 * @param currentUser 当前用户
	 * @param dto 
	 */
	void create(WoUser currentUser, UserDto dto);

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	UserDto getById(java.lang.String id);
	
	/**
	 * 更新对象
	 * @param dto
	 */
	void update(UserDto dto);
	
	/**
	 * 根据ids值删除多个对象
	 * @param ids
	 */
	void delete(java.lang.String[] ids);
}
