package com.qfedu.psys.service.impl;

import java.util.UUID;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import com.qfedu.psys.dto.RoleDto;
import com.qfedu.psys.dto.WoUser;
import com.qfedu.psys.po.Role;
import com.qfedu.psys.repository.RoleRepository;
import com.qfedu.psys.service.RoleService;

/**
 * PO实体Role对应的Service接口实现类.
 * @author cailei
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	/**
	 * 使用日志打印框架.
	 */
	private final static Logger LOG = LogManager.getLogger(RoleServiceImpl.class);
	
	/**
	 * 注入RoleRepository.
	 */
	@Resource // @Autowired
	private RoleRepository roleRepository;

	/**
	 * 列表
	 * @param currentUser 当前用户
	 * @param searchContent 查询内容文本
	 * @param page 页索引,从1开始
	 * @param size 一页的最大记录数
	 * @return
	 */
	@Override
	public WoPage<RoleDto> getPageData(WoUser currentUser, String searchContent, Long page, Long size) {
		// 从dao获取数据
		Pageable pageInput = PageRequest.of(page.intValue() - 1, size.intValue());
		Page<Role> pageData = null;
		if (WoUtil.isEmpty(searchContent)) {
			pageData = roleRepository.findAll(pageInput);
		} else {
			pageData = roleRepository.findAllByNameLike("%" + searchContent + "%", pageInput);
		}
		// 将Role转化为RoleDto
		WoPage<RoleDto> pr = RoleDto.getPageData(pageData.getContent(), pageData.getTotalElements());
		return pr.setPage(page);
	}

	/**
	 * 创建
	 * @param currentUser 当前用户
	 * @param dto 
	 */
	@Override
	public void create(WoUser currentUser, RoleDto dto) {
		Role po = dto.createPO();
		roleRepository.save(po);
	}

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	@Override
	public RoleDto getById(String id) {
		Role po = roleRepository.findById(id).get();
		return new RoleDto(po);
	}

	/**
	 * 更新对象
	 * @param dto
	 */
	@Override
	public void update(RoleDto dto) {
		Role po = roleRepository.findById(dto.getId()).get();
		dto.updatePO(po);
		roleRepository.save(po);
	}

	/**
	 * 根据ids值删除多个对象
	 * @param ids
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			roleRepository.deleteById(id);
		}
	}

}
