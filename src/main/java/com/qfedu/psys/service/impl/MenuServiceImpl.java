package com.qfedu.psys.service.impl;

import java.util.UUID;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import com.qfedu.psys.dto.MenuDto;
import com.qfedu.psys.dto.WoUser;
import com.qfedu.psys.po.Menu;
import com.qfedu.psys.repository.MenuRepository;
import com.qfedu.psys.service.MenuService;

/**
 * PO实体Menu对应的Service接口实现类.
 * @author cailei
 */
@Service
@Transactional
public class MenuServiceImpl implements MenuService {

	/**
	 * 使用日志打印框架.
	 */
	private final static Logger LOG = LogManager.getLogger(MenuServiceImpl.class);
	
	/**
	 * 注入MenuRepository.
	 */
	@Resource // @Autowired
	private MenuRepository menuRepository;

	/**
	 * 列表
	 * @param currentUser 当前用户
	 * @param searchContent 查询内容文本
	 * @param page 页索引,从1开始
	 * @param size 一页的最大记录数
	 * @return
	 */
	@Override
	public WoPage<MenuDto> getPageData(WoUser currentUser, String searchContent, Long page, Long size) {
		// 从dao获取数据
		Pageable pageInput = PageRequest.of(page.intValue() - 1, size.intValue(), Direction.ASC, "no");
		Page<Menu> pageData = null;
		if (WoUtil.isEmpty(searchContent)) {
			pageData = menuRepository.findAll(pageInput);
		} else {
			pageData = menuRepository.findAllByNameLike("%" + searchContent + "%", pageInput);
		}
		// 将Menu转化为MenuDto
		WoPage<MenuDto> pr = MenuDto.getPageData(pageData.getContent(), pageData.getTotalElements());
		return pr.setPage(page);
	}

	/**
	 * 创建
	 * @param currentUser 当前用户
	 * @param dto 
	 */
	@Override
	public void create(WoUser currentUser, MenuDto dto) {
		Menu po = dto.createPO();
		menuRepository.save(po);
	}

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	@Override
	public MenuDto getById(String id) {
		Menu po = menuRepository.findById(id).get();
		return new MenuDto(po);
	}

	/**
	 * 更新对象
	 * @param dto
	 */
	@Override
	public void update(MenuDto dto) {
		Menu po = menuRepository.findById(dto.getId()).get();
		dto.updatePO(po);
		menuRepository.save(po);
	}

	/**
	 * 根据ids值删除多个对象
	 * @param ids
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			menuRepository.deleteById(id);
		}
	}

}
