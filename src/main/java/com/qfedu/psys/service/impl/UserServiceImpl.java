package com.qfedu.psys.service.impl;

import java.util.UUID;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import com.qfedu.psys.dto.UserDto;
import com.qfedu.psys.dto.WoUser;
import com.qfedu.psys.po.User;
import com.qfedu.psys.repository.UserRepository;
import com.qfedu.psys.service.UserService;

/**
 * PO实体User对应的Service接口实现类.
 * @author cailei
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

	/**
	 * 使用日志打印框架.
	 */
	private final static Logger LOG = LogManager.getLogger(UserServiceImpl.class);
	
	/**
	 * 注入UserRepository.
	 */
	@Resource // @Autowired
	private UserRepository userRepository;

	/**
	 * 列表
	 * @param currentUser 当前用户
	 * @param searchContent 查询内容文本
	 * @param page 页索引,从1开始
	 * @param size 一页的最大记录数
	 * @return
	 */
	@Override
	public WoPage<UserDto> getPageData(WoUser currentUser, String searchContent, Long page, Long size) {
		// 从dao获取数据
		Pageable pageInput = PageRequest.of(page.intValue() - 1, size.intValue());
		Page<User> pageData = null;
		if (WoUtil.isEmpty(searchContent)) {
			pageData = userRepository.findAll(pageInput);
		} else {
			pageData = userRepository.findAllByLoginNameLike("%" + searchContent + "%", pageInput);
		}
		// 将User转化为UserDto
		WoPage<UserDto> pr = UserDto.getPageData(pageData.getContent(), pageData.getTotalElements());
		return pr.setPage(page);
	}

	/**
	 * 创建
	 * @param currentUser 当前用户
	 * @param dto 
	 */
	@Override
	public void create(WoUser currentUser, UserDto dto) {
		User po = dto.createPO();
		userRepository.save(po);
	}

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	@Override
	public UserDto getById(String id) {
		User po = userRepository.findById(id).get();
		return new UserDto(po);
	}

	/**
	 * 更新对象
	 * @param dto
	 */
	@Override
	public void update(UserDto dto) {
		User po = userRepository.findById(dto.getId()).get();
		dto.updatePO(po);
		userRepository.save(po);
	}

	/**
	 * 根据ids值删除多个对象
	 * @param ids
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			userRepository.deleteById(id);
		}
	}

}
