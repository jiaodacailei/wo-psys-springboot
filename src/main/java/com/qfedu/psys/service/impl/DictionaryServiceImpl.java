package com.qfedu.psys.service.impl;

import java.util.UUID;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import com.qfedu.psys.dto.DictionaryDto;
import com.qfedu.psys.dto.WoUser;
import com.qfedu.psys.po.Dictionary;
import com.qfedu.psys.repository.DictionaryRepository;
import com.qfedu.psys.service.DictionaryService;

/**
 * PO实体Dictionary对应的Service接口实现类.
 * @author cailei
 */
@Service
@Transactional
public class DictionaryServiceImpl implements DictionaryService {

	/**
	 * 使用日志打印框架.
	 */
	private final static Logger LOG = LogManager.getLogger(DictionaryServiceImpl.class);
	
	/**
	 * 注入DictionaryRepository.
	 */
	@Resource // @Autowired
	private DictionaryRepository dictionaryRepository;

	/**
	 * 列表
	 * @param currentUser 当前用户
	 * @param searchContent 查询内容文本
	 * @param page 页索引,从1开始
	 * @param size 一页的最大记录数
	 * @return
	 */
	@Override
	public WoPage<DictionaryDto> getPageData(WoUser currentUser, String searchContent, Long page, Long size) {
		// 从dao获取数据
		Pageable pageInput = PageRequest.of(page.intValue() - 1, size.intValue());
		Page<Dictionary> pageData = null;
		if (WoUtil.isEmpty(searchContent)) {
			pageData = dictionaryRepository.findAll(pageInput);
		} else {
			pageData = dictionaryRepository.findAllByDicTypeLike("%" + searchContent + "%", pageInput);
		}
		// 将Dictionary转化为DictionaryDto
		WoPage<DictionaryDto> pr = DictionaryDto.getPageData(pageData.getContent(), pageData.getTotalElements());
		return pr.setPage(page);
	}

	/**
	 * 创建
	 * @param currentUser 当前用户
	 * @param dto 
	 */
	@Override
	public void create(WoUser currentUser, DictionaryDto dto) {
		Dictionary po = dto.createPO();
		dictionaryRepository.save(po);
	}

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	@Override
	public DictionaryDto getById(String id) {
		Dictionary po = dictionaryRepository.findById(id).get();
		return new DictionaryDto(po);
	}

	/**
	 * 更新对象
	 * @param dto
	 */
	@Override
	public void update(DictionaryDto dto) {
		Dictionary po = dictionaryRepository.findById(dto.getId()).get();
		dto.updatePO(po);
		dictionaryRepository.save(po);
	}

	/**
	 * 根据ids值删除多个对象
	 * @param ids
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			dictionaryRepository.deleteById(id);
		}
	}

}
