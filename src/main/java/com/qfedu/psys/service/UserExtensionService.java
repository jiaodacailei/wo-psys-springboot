package com.qfedu.psys.service;
import com.qfedu.psys.dto.WoUser;

/**
 * 角色扩展服务，需要上层去实现
 * @author cailei
 *
 */
public interface UserExtensionService {
	
	/**
	 * @param u
	 * @return
	 */
	public void setUserInfo(WoUser u);
}
