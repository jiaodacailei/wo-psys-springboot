package com.qfedu.department.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.qfedu.department.po.Staff;

/**
 * 实体Staff基于spring-data的DAO接口.
 * @author cailei
 */
public interface StaffRepository extends PagingAndSortingRepository<Staff, java.lang.String>{
	
	Page<Staff> findAllByNameLike(java.lang.String searchContent, Pageable pageInput);

	Staff findByUserId(String id);
}
