package com.qfedu.department.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.qfedu.department.po.Position;

/**
 * 实体Position基于spring-data的DAO接口.
 * @author cailei
 */
public interface PositionRepository extends PagingAndSortingRepository<Position, java.lang.String>{
	
	Page<Position> findAllByNameLike(java.lang.String searchContent, Pageable pageInput);
}
