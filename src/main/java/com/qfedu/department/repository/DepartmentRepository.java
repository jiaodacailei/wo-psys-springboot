package com.qfedu.department.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.qfedu.department.po.Department;

/**
 * 实体Department基于spring-data的DAO接口.
 * @author cailei
 */
public interface DepartmentRepository extends PagingAndSortingRepository<Department, java.lang.String>{
	
	Page<Department> findAllByNameLike(java.lang.String searchContent, Pageable pageInput);
}
