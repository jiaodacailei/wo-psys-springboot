package com.qfedu.department.dto;

import java.util.ArrayList;
import java.util.List;

import com.qfedu.common.entity.WoPage;
import com.qfedu.department.po.Staff;

/**
 * PO实体Staff对应的DTO类.
 * @author cailei
 */
public class StaffDto extends StaffDto_ {

	/**
	 * 无参构造函数
	 */
	public StaffDto() {
		super ();
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public StaffDto(Staff po) {
		super (po);
	}
	
	/**
	 * 将po列表数据转化为dto列表数据
	 * @param pos
	 * @return
	 */
	public static List<StaffDto> getDtos (List<Staff> pos) {
		List<StaffDto> rs = new ArrayList<StaffDto>();
		for (Staff r : pos) {
			StaffDto dto = new StaffDto(r);
			rs.add(dto);
		}
		return rs;
	}
	
	/**
	 * 将分页po数据转化为dto分页数据
	 * @param pos
	 * @param total
	 * @return
	 */
	public static WoPage<StaffDto> getPageData(List<Staff> pos, Long total) {
		WoPage<StaffDto> puDto = new WoPage<StaffDto>(getDtos(pos), total);
		return puDto;
	}
}
