package com.qfedu.department.dto;

import java.util.ArrayList;
import java.util.List;

import com.qfedu.common.entity.WoPage;
import com.qfedu.department.po.Department;

/**
 * PO实体Department对应的DTO类.
 * @author cailei
 */
public class DepartmentDto extends DepartmentDto_ {

	/**
	 * 无参构造函数
	 */
	public DepartmentDto() {
		super ();
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public DepartmentDto(Department po) {
		super (po);
	}
	
	/**
	 * 将po列表数据转化为dto列表数据
	 * @param pos
	 * @return
	 */
	public static List<DepartmentDto> getDtos (List<Department> pos) {
		List<DepartmentDto> rs = new ArrayList<DepartmentDto>();
		for (Department r : pos) {
			DepartmentDto dto = new DepartmentDto(r);
			rs.add(dto);
		}
		return rs;
	}
	
	/**
	 * 将分页po数据转化为dto分页数据
	 * @param pos
	 * @param total
	 * @return
	 */
	public static WoPage<DepartmentDto> getPageData(List<Department> pos, Long total) {
		WoPage<DepartmentDto> puDto = new WoPage<DepartmentDto>(getDtos(pos), total);
		return puDto;
	}
}
