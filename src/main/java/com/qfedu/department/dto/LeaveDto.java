package com.qfedu.department.dto;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.qfedu.common.entity.WoActivitiDto;

public class LeaveDto extends WoActivitiDto{
	
	private final static Logger LOG = LogManager.getLogger(LeaveDto.class);
	
	private String applyId;
	
	private String applyName;
	
	private Long applyHours;
	
	private String applyType;
	
	private String applyReason;

	public String getApplyId() {
		return applyId;
	}

	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}

	public String getApplyName() {
		return applyName;
	}

	public void setApplyName(String applyName) {
		this.applyName = applyName;
	}

	public Long getApplyHours() {
		return applyHours;
	}

	public void setApplyHours(Long applyHours) {
		this.applyHours = applyHours;
	}

	public String getApplyType() {
		return applyType;
	}
	
	public String getApplyTypeName() {
		if ("1".equals(applyType)) {
			return "事假";
		}
		return "调休";
	}
	
	public void setApplyType(String applyType) {
		this.applyType = applyType;
	}

	public String getApplyReason() {
		return applyReason;
	}

	public void setApplyReason(String applyReason) {
		this.applyReason = applyReason;
	}
	
}
