package com.qfedu.department.dto;

import java.util.ArrayList;
import java.util.List;

import com.qfedu.common.entity.WoPage;
import com.qfedu.department.po.Position;

/**
 * PO实体Position对应的DTO类.
 * @author cailei
 */
public class PositionDto extends PositionDto_ {

	/**
	 * 无参构造函数
	 */
	public PositionDto() {
		super ();
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public PositionDto(Position po) {
		super (po);
	}
	
	/**
	 * 将po列表数据转化为dto列表数据
	 * @param pos
	 * @return
	 */
	public static List<PositionDto> getDtos (List<Position> pos) {
		List<PositionDto> rs = new ArrayList<PositionDto>();
		for (Position r : pos) {
			PositionDto dto = new PositionDto(r);
			rs.add(dto);
		}
		return rs;
	}
	
	/**
	 * 将分页po数据转化为dto分页数据
	 * @param pos
	 * @param total
	 * @return
	 */
	public static WoPage<PositionDto> getPageData(List<Position> pos, Long total) {
		WoPage<PositionDto> puDto = new WoPage<PositionDto>(getDtos(pos), total);
		return puDto;
	}
}
