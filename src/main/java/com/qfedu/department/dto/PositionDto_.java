package com.qfedu.department.dto;

import java.util.ArrayList;
import java.util.List;
import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import com.qfedu.department.po.Position;

/**
 * PO实体Position对应的DTO父类.这是自动生成的代码,请不要修改.如要添加属性或者方法,请在其子类DTO中修改.
 * @author cailei
 */
class PositionDto_ {

	/**
	 * 主键id
	 */
	private java.lang.String id;

	/**
	 * 属性name
	 */
	private java.lang.String name;
	/**
	 * 属性description
	 */
	private java.lang.String description;

	

	
	/**
	 * 无参构造函数
	 */
	public PositionDto_() {
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public PositionDto_(Position po) {
			// 设置主键id
		this.id = po.getId();
				// 设置属性name
		this.name = po.getName();
			// 设置属性description
		this.description = po.getDescription();
		
				}

	
	/**
	 * 主键id的getter方法
	 */
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 * 主键id的setter方法
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	
	/**
	 * 属性name的getter方法
	 */
	public java.lang.String getName() {
		return this.name;
	}

	/**
	 * 属性name的setter方法
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}
	
	/**
	 * 属性description的getter方法
	 */
	public java.lang.String getDescription() {
		return this.description;
	}

	/**
	 * 属性description的setter方法
	 */
	public void setDescription(java.lang.String description) {
		this.description = description;
	}


	/**
	 * 将当前对象转化为po
	 * @return
	 */
	public Position createPO() {
		Position po = new Position();
			// 设置PO主键id
					if (WoUtil.isEmpty(this.id) && this.id instanceof String) {
				po.setId(java.util.UUID.randomUUID().toString());
			} else {
				po.setId(this.id);
			}
						// 设置PO属性name
				po.setName(this.name);
					// 设置PO属性description
				po.setDescription(this.description);
				
				return po;
	}

	/**
	 * @param po
	 */
	public void updatePO(Position po) {
			// 设置PO属性name
		po.setName(this.name);
			// 设置PO属性description
		po.setDescription(this.description);
		
			}
}
