package com.qfedu.department.dto;

import java.util.ArrayList;
import java.util.List;
import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import com.qfedu.department.po.Staff;
import com.qfedu.psys.po.User;
import com.qfedu.department.po.Department;
import com.qfedu.department.po.Position;

/**
 * PO实体Staff对应的DTO父类.这是自动生成的代码,请不要修改.如要添加属性或者方法,请在其子类DTO中修改.
 * @author cailei
 */
class StaffDto_ {

	/**
	 * 主键id
	 */
	private java.lang.String id;

	/**
	 * 属性name
	 */
	private java.lang.String name;
	/**
	 * 属性gender
	 */
	private java.lang.String gender;
	/**
	 * 属性mobile
	 */
	private java.lang.String mobile;
	/**
	 * 属性birthday
	 */
	private java.util.Date birthday;

	/**
	 * 对应PO的positions属性,多对多关联实体Position的主键值,如有多个以逗号隔开
	 */
	private String positionsId = "";

	/**
	 * 对应PO的positions属性,多对多关联实体Position的name属性值,如有多个以逗号隔开
	 */
	private String positionsName = "";
	
	/**
	 * 对应PO的department属性,多对一关联实体Department的主键值
	 */
	private java.lang.String departmentId;

	/**
	 * 对应PO的department属性,多对一关联实体Department的name属性值
	 */
	private java.lang.String departmentName;

	
	/**
	 * 无参构造函数
	 */
	public StaffDto_() {
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public StaffDto_(Staff po) {
			// 设置主键id
		this.id = po.getId();
				// 设置属性name
		this.name = po.getName();
			// 设置属性gender
		this.gender = po.getGender();
			// 设置属性mobile
		this.mobile = po.getMobile();
			// 设置属性birthday
		this.birthday = po.getBirthday();
		
			// 设置DTO的positions属性值
		for (Position p : po.getPositions()) {
			if (!"".equals(positionsId)) {
				positionsId += ",";
				positionsName += ",";
			}
			positionsId += p.getId();
			positionsName += p.getName();
		}
				// 设置DTO的department属性值
		if (po.getDepartment() != null) {
			this.departmentId = po.getDepartment().getId();
			this.departmentName = po.getDepartment().getName();
		}
			}

	
	/**
	 * 主键id的getter方法
	 */
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 * 主键id的setter方法
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	
	/**
	 * 属性name的getter方法
	 */
	public java.lang.String getName() {
		return this.name;
	}

	/**
	 * 属性name的setter方法
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}
	
	/**
	 * 属性gender的getter方法
	 */
	public java.lang.String getGender() {
		return this.gender;
	}

	/**
	 * 属性gender的setter方法
	 */
	public void setGender(java.lang.String gender) {
		this.gender = gender;
	}
	
	/**
	 * 属性mobile的getter方法
	 */
	public java.lang.String getMobile() {
		return this.mobile;
	}

	/**
	 * 属性mobile的setter方法
	 */
	public void setMobile(java.lang.String mobile) {
		this.mobile = mobile;
	}
	
	/**
	 * 属性birthday的getter方法
	 */
	public java.util.Date getBirthday() {
		return this.birthday;
	}

	/**
	 * 属性birthday的setter方法
	 */
	public void setBirthday(java.util.Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * 属性positionsId的getter方法
	 */
	public String getPositionsId() {
		return positionsId;
	}

	/**
	 * 属性positionsId的setter方法
	 */
	public void setPositionsId(String positionsId) {
		this.positionsId = positionsId;
	}

	/**
	 * 属性positionsName的getter方法
	 */
	public String getPositionsName() {
		return positionsName;
	}

	/**
	 * 属性positionsName的setter方法
	 */
	public void setPositionsName(String positionsName) {
		this.positionsName = positionsName;
	}
	/**
	 * 属性departmentId的getter方法
	 */
	public java.lang.String getDepartmentId() {
		return departmentId;
	}

	/**
	 * 属性departmentId的setter方法
	 */
	public void setDepartmentId(java.lang.String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * 属性departmentName的getter方法
	 */
	public java.lang.String getDepartmentName() {
		return departmentName;
	}

	/**
	 * 属性departmentName的setter方法
	 */
	public void setDepartmentName(java.lang.String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * 将当前对象转化为po
	 * @return
	 */
	public Staff createPO() {
		Staff po = new Staff();
			// 设置PO主键id
					if (WoUtil.isEmpty(this.id) && this.id instanceof String) {
				po.setId(java.util.UUID.randomUUID().toString());
			} else {
				po.setId(this.id);
			}
						// 设置PO属性name
				po.setName(this.name);
					// 设置PO属性gender
				po.setGender(this.gender);
					// 设置PO属性mobile
				po.setMobile(this.mobile);
					// 设置PO属性birthday
				po.setBirthday(this.birthday);
				
			// 设置PO的positions属性值
		List<Position> positions = new ArrayList<Position>();
		String[] positionsIdArray = WoUtil.splitIds(positionsId);
		for (String id : positionsIdArray) {
			Position p = new Position ();
			p.setId(id);
			positions.add(p);
		}
		po.setPositions(positions);
				// 设置关系数据
		Department department = new Department();
		if (!WoUtil.isEmpty(departmentId)) {
			department.setId(this.departmentId);
			po.setDepartment(department);
		}
			return po;
	}

	/**
	 * @param po
	 */
	public void updatePO(Staff po) {
			// 设置PO属性name
		po.setName(this.name);
			// 设置PO属性gender
		po.setGender(this.gender);
			// 设置PO属性mobile
		po.setMobile(this.mobile);
			// 设置PO属性birthday
		po.setBirthday(this.birthday);
		
			// 设置PO的positions属性值
		List<Position> positions = new ArrayList<Position>();
		String[] positionsIdArray = WoUtil.splitIds(positionsId);
		for (String id : positionsIdArray) {
			Position p = new Position ();
			p.setId(id);
			positions.add(p);
		}
		po.setPositions(positions);
				// 设置关系数据
		Department department = new Department();
		if (!WoUtil.isEmpty(departmentId)) {
			department.setId(this.departmentId);
			po.setDepartment(department);
		}
		}
}
