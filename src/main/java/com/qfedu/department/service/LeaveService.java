package com.qfedu.department.service;
import com.qfedu.common.entity.WoPage;
import com.qfedu.department.dto.LeaveDto;
import com.qfedu.psys.dto.WoUser;

public interface LeaveService {

	String PROC_DEF_KEY = "qfLeave";
	
	void create(WoUser currentUser, LeaveDto dto);

	WoPage<LeaveDto> getPageData(WoUser currentUser, String searchContent, Long page, Long size);

}
