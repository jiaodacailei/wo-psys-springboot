package com.qfedu.department.service.impl;

import java.util.UUID;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import com.qfedu.department.dto.PositionDto;
import com.qfedu.psys.dto.WoUser;
import com.qfedu.department.po.Position;
import com.qfedu.department.repository.PositionRepository;
import com.qfedu.department.service.PositionService;

/**
 * PO实体Position对应的Service接口实现类.
 * @author cailei
 */
@Service
@Transactional
public class PositionServiceImpl implements PositionService {

	/**
	 * 使用日志打印框架.
	 */
	private final static Logger LOG = LogManager.getLogger(PositionServiceImpl.class);
	
	/**
	 * 注入PositionRepository.
	 */
	@Resource // @Autowired
	private PositionRepository positionRepository;

	/**
	 * 列表
	 * @param currentUser 当前用户
	 * @param searchContent 查询内容文本
	 * @param page 页索引,从1开始
	 * @param size 一页的最大记录数
	 * @return
	 */
	@Override
	public WoPage<PositionDto> getPageData(WoUser currentUser, String searchContent, Long page, Long size) {
		// 从dao获取数据
		Pageable pageInput = PageRequest.of(page.intValue() - 1, size.intValue());
		Page<Position> pageData = null;
		if (WoUtil.isEmpty(searchContent)) {
			pageData = positionRepository.findAll(pageInput);
		} else {
			pageData = positionRepository.findAllByNameLike("%" + searchContent + "%", pageInput);
		}
		// 将Position转化为PositionDto
		WoPage<PositionDto> pr = PositionDto.getPageData(pageData.getContent(), pageData.getTotalElements());
		return pr.setPage(page);
	}

	/**
	 * 创建
	 * @param currentUser 当前用户
	 * @param dto 
	 */
	@Override
	public void create(WoUser currentUser, PositionDto dto) {
		Position po = dto.createPO();
		positionRepository.save(po);
	}

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	@Override
	public PositionDto getById(String id) {
		Position po = positionRepository.findById(id).get();
		return new PositionDto(po);
	}

	/**
	 * 更新对象
	 * @param dto
	 */
	@Override
	public void update(PositionDto dto) {
		Position po = positionRepository.findById(dto.getId()).get();
		dto.updatePO(po);
		positionRepository.save(po);
	}

	/**
	 * 根据ids值删除多个对象
	 * @param ids
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			positionRepository.deleteById(id);
		}
	}

}
