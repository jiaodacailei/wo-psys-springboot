package com.qfedu.department.service.impl;

import java.util.UUID;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import com.qfedu.department.dto.StaffDto;
import com.qfedu.psys.dto.WoUser;
import com.qfedu.department.po.Staff;
import com.qfedu.department.repository.StaffRepository;
import com.qfedu.department.service.StaffService;

/**
 * PO实体Staff对应的Service接口实现类.
 * @author cailei
 */
@Service
@Transactional
public class StaffServiceImpl implements StaffService {

	/**
	 * 使用日志打印框架.
	 */
	private final static Logger LOG = LogManager.getLogger(StaffServiceImpl.class);
	
	/**
	 * 注入StaffRepository.
	 */
	@Resource // @Autowired
	private StaffRepository staffRepository;

	/**
	 * 列表
	 * @param currentUser 当前用户
	 * @param searchContent 查询内容文本
	 * @param page 页索引,从1开始
	 * @param size 一页的最大记录数
	 * @return
	 */
	@Override
	public WoPage<StaffDto> getPageData(WoUser currentUser, String searchContent, Long page, Long size) {
		// 从dao获取数据
		Pageable pageInput = PageRequest.of(page.intValue() - 1, size.intValue());
		Page<Staff> pageData = null;
		if (WoUtil.isEmpty(searchContent)) {
			pageData = staffRepository.findAll(pageInput);
		} else {
			pageData = staffRepository.findAllByNameLike("%" + searchContent + "%", pageInput);
		}
		// 将Staff转化为StaffDto
		WoPage<StaffDto> pr = StaffDto.getPageData(pageData.getContent(), pageData.getTotalElements());
		return pr.setPage(page);
	}

	/**
	 * 创建
	 * @param currentUser 当前用户
	 * @param dto 
	 */
	@Override
	public void create(WoUser currentUser, StaffDto dto) {
		Staff po = dto.createPO();
		staffRepository.save(po);
	}

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	@Override
	public StaffDto getById(String id) {
		Staff po = staffRepository.findById(id).get();
		return new StaffDto(po);
	}

	/**
	 * 更新对象
	 * @param dto
	 */
	@Override
	public void update(StaffDto dto) {
		Staff po = staffRepository.findById(dto.getId()).get();
		dto.updatePO(po);
		staffRepository.save(po);
	}

	/**
	 * 根据ids值删除多个对象
	 * @param ids
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			staffRepository.deleteById(id);
		}
	}

}
