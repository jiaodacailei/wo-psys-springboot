package com.qfedu.department.controller;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.entity.WoResultCode;
import com.qfedu.common.entity.WoSelectorParams;
import com.qfedu.common.exception.handler.WoControllerExceptionHandler;
import com.qfedu.psys.util.PSysConstant;
import com.qfedu.psys.util.PSysUtil;
import com.qfedu.psys.dto.MenuDto;
import com.qfedu.psys.service.CoreService;
import com.qfedu.department.dto.StaffDto;
import com.qfedu.department.service.StaffService;
/**
 * 人员对应控制器
 * @author cailei
 */
@Controller
@SessionAttributes(names=PSysConstant.SESSION_USER)
@RequestMapping ("/department/staff")
public class StaffController {
	private final static Logger LOG = LogManager.getLogger(StaffController.class);
	
	/**
	 * 注入StaffService.
	 */
	@Resource
	private StaffService staffService;
	
	/**
	 * 注入MenuService
	 */
	@Resource
	private CoreService coreService;
	
	/**
	 * 注入异常处理器.
	 */
	@Resource
	private WoControllerExceptionHandler exceptionHandler;
	
	/**
	 * 加载整个人员管理页面.
	 * @param map
	 * @return
	 */
	@RequestMapping ("")
	public String main (Map<String, Object> map) {
		return exceptionHandler.toLoginView(() -> {
			List<MenuDto> menus = coreService.getMenus(PSysUtil.getCurrentUser(map), "menu-department", "menu-staff");
			map.put("menus", menus);
		}, map, "department/staff");
	}
	
	/**
	 * 前端加载整个人员管理页面后,会发送AJAX请求加载人员列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/list")
	public String list(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<StaffDto> pageData = staffService.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "department/staff-list");
	}
	
	/**
	 * 加载人员创建表单.
	 * @param map
	 * @return
	 */
	@GetMapping("/create")
	public String create(Map<String, Object> map) {
		return "department/staff-create";
	}
	
	/**
	 * 提交人员创建表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/create")
	@ResponseBody
	public WoResultCode create(StaffDto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> staffService.create(PSysUtil.getCurrentUser(map), dto), "创建人员成功！");
	}
	
	/**
	 * 加载人员修改表单.
	 * @param id
	 * @param map
	 * @return
	 */
	@GetMapping("/update")
	public String update(String id, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			map.put("formData", staffService.getById(id));
		}, map, "department/staff-update");
	}
	
	/**
	 * 提交人员修改表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/update")
	@ResponseBody
	public WoResultCode update(StaffDto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> staffService.update(dto), "修改人员成功！");
	}
	
	/**
	 * 批量删除人员.
	 * @param id
	 * @param map
	 * @return
	 */
	@PostMapping("/delete")
	@ResponseBody
	public WoResultCode delete(String[] id, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> staffService.delete(id), "删除人员成功！");
	}
	
	/**
	 * 加载人员选择器页面.
	 * @param params
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector")
	public String loadSelector(WoSelectorParams params, Map<String, Object> map) {
		map.put("params", params);
		return "department/staff-selector";
	}
	
	/**
	 * 前端加载整个人员选择器页面后,会发送AJAX请求加载人员选择器列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector/list")
	public String selectorList(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<StaffDto> pageData = staffService.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "department/staff-selector-list");
	}
}
