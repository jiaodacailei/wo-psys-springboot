package com.qfedu.department.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 职务
 * @author cailei
 * @date 2018年11月6日
 */
@Entity
@Table(name = "dpt_position")
public class Position {
	private final static Logger LOG = LogManager.getLogger(Position.class);
	
	/**
	 * ID
	 */
	@Id
	@Column(length = 50)
	private String id;
	
	/**
	 * 名称
	 */
	@Column(length = 50)
	private String name;
	
	/**
	 * 描述
	 */
	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
