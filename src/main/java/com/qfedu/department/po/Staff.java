package com.qfedu.department.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.qfedu.psys.po.User;

/**
 * 人员
 * 
 * @author cailei
 * @date 2018年11月6日
 */
@Entity
@Table(name = "dpt_staff")
public class Staff {
	private final static Logger LOG = LogManager.getLogger(Staff.class);

	/**
	 * ID
	 */
	@Id
	@Column(length = 50)
	private String id;

	/**
	 * 姓名
	 */
	@Column(length = 50)
	private String name;

	/**
	 * 性别
	 */
	@Column(length = 5)
	private String gender;

	/**
	 * 手机号，用于登录名
	 */
	@Column(length = 30)
	private String mobile;
	
	/**
	 * 生日
	 */
	@Temporal(TemporalType.DATE)
	private Date birthday;

	/**
	 * 用户
	 */
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	/**
	 * 部门
	 */
	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;

	/**
	 * 职务
	 */
	@ManyToMany
	@JoinTable(name = "dpt_staff_position", joinColumns = @JoinColumn(name = "staff_id"), 
		inverseJoinColumns = @JoinColumn(name = "position_id"))
	private List<Position> positions = new ArrayList<Position>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<Position> getPositions() {
		return positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
}
