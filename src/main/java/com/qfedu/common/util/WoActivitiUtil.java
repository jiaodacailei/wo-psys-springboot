package com.qfedu.common.util;

import javax.annotation.Resource;

import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricVariableInstance;
import org.springframework.stereotype.Component;

@Component
public class WoActivitiUtil {

	@Resource
	private HistoryService historyService;
	
	/**
	 * @param processInstanceId
	 * @param variableKey
	 * @return
	 */
	public Object getVariableValue (String processInstanceId, String variableKey) {
		HistoricVariableInstance variable = historyService.createHistoricVariableInstanceQuery()
				.processInstanceId(processInstanceId).variableName(variableKey).singleResult();
		if (variable == null) {
			return null;
		}
		return variable.getValue();
	}
	
	/**
	 * @param processInstanceId
	 * @param variableKey
	 * @return
	 */
	public String getVariableStringValue (String processInstanceId, String variableKey) {
		Object val = getVariableValue(processInstanceId, variableKey);
		if (val == null) {
			return null;
		}
		return val.toString();
	}
}
