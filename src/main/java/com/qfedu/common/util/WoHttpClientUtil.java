package com.qfedu.common.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.qfedu.common.WoException;

public class WoHttpClientUtil {

	private final static Logger LOG = LogManager.getLogger(WoHttpClientUtil.class);

	/**
	 * 模拟请求
	 * 
	 * @param url      资源地址
	 * @param map      参数列表
	 * @param encoding 编码
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	public static String send(String url, Map<String, String> map, String encoding) {
		try {
			String body = "";

			// 创建httpclient对象
			CloseableHttpClient client = HttpClients.createDefault();
			// 创建post方式请求对象
			HttpPost httpPost = new HttpPost(url);

			// 装填参数
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if (map != null) {
				for (Entry<String, String> entry : map.entrySet()) {
					nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
				}
			}
			// 设置参数到请求对象中
			httpPost.setEntity(new UrlEncodedFormEntity(nvps, encoding));

			LOG.info("请求地址：" + url);
			LOG.info("请求参数：" + nvps.toString());

			// 设置header信息
			// 指定报文头【Content-type】、【User-Agent】
			httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
			httpPost.setHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

			// 执行请求操作，并拿到结果（同步阻塞）
			CloseableHttpResponse response = client.execute(httpPost);
			// 获取结果实体
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				// 按指定编码转换结果实体为String类型
				body = EntityUtils.toString(entity, encoding);
			}
			EntityUtils.consume(entity);
			// 释放链接
			response.close();
			return body;
		} catch (Exception e) {
			throw new WoException (e, WoConstant.ERR_HTTP_POST, url);
		}
	}

	public static String send(String url, Map<String, String> map) {
		return send(url, map, "UTF-8");
	}
	
	public static void main (String [] args) throws ParseException, IOException {
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "#============================以下为描述头定义==========================&hh&V: 1.0&hh&B: test&hh&B: test&hh&Z: tes 词&hh&Z: te 曲&hh&Z: tsg&hh&D: E#&hh&P: 4/4&hh&J: 133&hh&#============================以下开始简谱主体==========================&hh&Q: 7 2 3 4 | 1 2 3 3 | |&hh&C: 这是歌词 v");
		map.put("pageConfig", "{\"page\":\"A4\",\"margin_top\":\"80\",\"margin_bottom\":\"80\",\"margin_left\":\"80\",\"margin_right\":\"80\",\"biaoti_font\":\"Microsoft YaHei\",\"shuzi_font\":\"b\",\"geci_font\":\"Microsoft YaHei\",\"height_quci\":\"13\",\"height_cici\":\"10\",\"height_ciqu\":\"40\",\"height_shengbu\":\"0\",\"biaoti_size\":\"36\",\"fubiaoti_size\":\"20\",\"geci_size\":\"18\",\"body_margin_top\":\"40\",\"lianyinxian_type\":\"0\"}");
		map.put("pageNum", "0");
		String body = send ("http://zhipu.lezhi99.com/Zhipu-draw", map, "UTF-8");
		LOG.info(body);
	}
	
}
