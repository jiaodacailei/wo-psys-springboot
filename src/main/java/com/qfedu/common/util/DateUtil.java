package com.qfedu.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 时间工具类
 * @author cailei
 * @date Sep 9, 2018
 */
public class DateUtil {
	
	private final static Logger LOG = LogManager.getLogger(DateUtil.class);
	
	/**
	 * 获取两个时间之间的年差值
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static int getDifferenceYear (Date d1, Date d2) {
		if (d1 == null) {
			LOG.warn("d1 is null...");
			return 0;
		}
		if (d2 == null) {
			d2 = new Date();
			LOG.warn("d2 is null, set to new Date()...");
		}
		long d = d1.getTime() - d2.getTime();
		if (d <= 0) {
			LOG.warn("d1 <= d2...");
			return 0;
		}
		return (int)(d/(24*60*60*1000)/365);
	}
	
	/**
	 * 获取时间d1和当前时间的年差值
	 * @param d1
	 * @return
	 */
	public static int getCurrentDifferenceYear (Date d1) {
		return getDifferenceYear (new Date(), d1);
	}
	
	/**
	 * 格式化日期
	 * 
	 * @param dateStr String 字符型日期
	 * @param format String 格式
	 * @return Date 日期
	 */
	public static Date parseDate(String dateStr, String format) {
		Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(format);
			date = dateFormat.parse(dateStr);
		} catch (Exception e) {
			LOG.error("", e);
		}
		return date;
	}
	
	public static void main (String[] args) {
		LOG.info(DateUtil.getCurrentDifferenceYear(parseDate("2000-09-09","yyyy-MM-dd")));
	}
}
