package com.qfedu.common.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.qfedu.common.WoException;

public class WoUtil {

	private final static Logger LOG = LogManager.getLogger(WoUtil.class);

	/**
	 * @param obj
	 * @return
	 */
	public static Boolean isEmpty(Object obj) {
		if (obj == null) {
			return true;
		}
		if (obj instanceof Map) {
			Map map = (Map) obj;
			if (map.keySet().size() == 0) {
				return true;
			}
			return false;
		}
		if (obj instanceof MultipartFile) {
			return WoUtil.isEmpty(((MultipartFile)obj).getOriginalFilename());
		}
		if (obj instanceof String) {
			if ("".equals(obj) || "".equals(obj.toString().trim())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 把id字符串解析为非null字符串数组。
	 * 
	 * @param strIds
	 *            以逗号隔开的id字符串
	 * @return
	 */
	public static String[] splitIds(String strIds) {
		String[] ids = new String[0];
		if (!WoUtil.isEmpty(strIds)) {
			ids = strIds.split(",");
		}
		return ids;
	}

	/**
	 * 获取value对应的模糊匹配表达式，例如：value是'vvv'，则表达式为'%vvv%'
	 * 
	 * @param value
	 * @return
	 */
	public static String getLikeValue(String value) {
		String val = "%";
		if (!WoUtil.isEmpty(value)) {
			val = "%" + value + "%";
		}
		return val;
	}

	/**
	 * if len is 4 and idx is 23 then re is 0023.
	 * 
	 * @param len
	 * @param idx
	 * @return re
	 */
	public static String generateFixedLenNo(int len, int idx) {
		return String.format("%0" + len + "d", idx);
		/*
		 * String re = String.valueOf(idx); if (re.length() >= len) { return
		 * re.substring(0, len); } len = len - re.length(); for (int i = 0; i <
		 * len; i++) { re = '0' + re; } return re;
		 */
	}

	/**
	 * 生成uuid
	 * 
	 * @return
	 */
	public static String uuid() {
		return UUID.randomUUID().toString();
	}

	/**
	 * @param objs
	 * @param obj
	 * @return
	 */
	public static Boolean contains(Object[] objs, Object obj) {
		for (Object o : objs) {
			if (o.equals(obj)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param objs
	 * @param obj
	 * @return
	 */
	public static Boolean contains(Collection objs, Object obj) {
		for (Object o : objs) {
			if (o.equals(obj)) {
				return true;
			}
		}
		return false;
	}

	private static ApplicationContext context;

	public static ApplicationContext getContext() {
		return context;
	}

	public static void setContext(ApplicationContext context) {
		WoUtil.context = context;
	}

	/**
	 * @param name
	 * @param cls
	 * @return
	 */
	public static <T> T getBean(String name, Class<T> cls) {
		// bean名称：woSceneRoleExtension + bean类型：IRoleExtensionService
		return context.getBean(name, cls);
	}

	/**
	 * @param name
	 * @param cls
	 * @return
	 */
	public static <T> T getBean(Class<T> cls) {
		// bean名称：woSceneRoleExtension + bean类型：IRoleExtensionService
		return context.getBean(cls);
	}

	/**
	 * @param s
	 * @return
	 */
	public static String getUpperFirstChar(String s) {
		char[] cs = s.toCharArray();
		cs[0] -= 32;
		return String.valueOf(cs);
	}

	/**
	 * 获取web项目根目录
	 * 
	 * @return
	 */
	public static String getWoRoot() {
		return System.getProperty("wo.root");
	}
	
	/**
	 * 将字符串通过MD5算法散列化
	 * @param salt
	 * @param str
	 * @return
	 */
	public static String getMD5(String salt, String str) {
		if (WoUtil.isEmpty(salt)) {
			throw new WoException (WoConstant.ERR_MD5_SALT);
		}
		if (WoUtil.isEmpty(str)) {
			throw new WoException (WoConstant.ERR_MD5_STR);
		}
        try {
            // 生成一个MD5加密计算摘要
            MessageDigest md = MessageDigest.getInstance("MD5");// MD5 SHA-256...
            // 计算md5函数
            md.update((salt + str).getBytes());
            // digest()最后确定返回md5 hash值，返回值为8位字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            return new BigInteger(1, md.digest()).toString(16);
        } catch (Exception e) {
        	throw new WoException (e, WoConstant.ERR_MD5);
        }
    }

	
	public static void main (String[] args) {
		LOG.info(getMD5 ("admin", "123456"));
	}
}