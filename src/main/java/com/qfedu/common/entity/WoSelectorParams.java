package com.qfedu.common.entity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 弹出选择器列表div时，需要传递的参数
 * @author cailei
 *
 */
public class WoSelectorParams {
	private final static Logger LOG = LogManager.getLogger(WoSelectorParams.class);
	
	/**
	 * id值，如果多个逗号隔开
	 */
	private String selectedIds;
	
	/**
	 * name值，如果多个逗号隔开
	 */
	private String selectedNames;
	
	/**
	 * 回调方法名称，选择器列表点击确定按钮时调用
	 */
	private String callback;

	public String getSelectedIds() {
		return selectedIds;
	}

	public void setSelectedIds(String selectedIds) {
		this.selectedIds = selectedIds;
	}

	public String getSelectedNames() {
		return selectedNames;
	}

	public void setSelectedNames(String selectedNames) {
		this.selectedNames = selectedNames;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}
	
}
