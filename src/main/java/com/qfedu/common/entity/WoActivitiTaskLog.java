package com.qfedu.common.entity;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricFormProperty;
import org.activiti.engine.history.HistoricTaskInstance;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 流程任务日志实体
 * @author cailei
 * @date 2018年9月15日
 */
public class WoActivitiTaskLog {
	private final static Logger LOG = LogManager.getLogger(WoActivitiTaskLog.class);
	
	/**
	 * 当前任务实例ID
	 */
	private String taskId;
	
	/**
	 * 当前任务定义ID,*.bpmn中的taskId
	 */
	private String taskKey;
	
	/**
	 * 当前任务名称
	 */
	private String taskName;
	
	/**
	 * 任务执行人
	 */
	private String taskAssignee;
	
	/**
	 * 任务开始时间
	 */
	private Date startTime;
	
	/**
	 * 任务结束时间
	 */
	private Date endTime;

	/**
	 * 任务相关的表单数据
	 */
	private Map<String, String> formData = new HashMap<String, String>();
	
	/**
	 * @param hti 历史任务
	 * @param hvis 历史任务的表单数据
	 */
	public WoActivitiTaskLog (HistoricTaskInstance hti, List<HistoricDetail> details) {
		this.setEndTime(hti.getEndTime());
		this.setStartTime(hti.getStartTime());
		this.setTaskAssignee(hti.getAssignee());
		this.setTaskId(hti.getId());
		this.setTaskKey(hti.getTaskDefinitionKey());
		this.setTaskName(hti.getName());
		for (HistoricDetail detail : details) {
			if (!(detail instanceof HistoricFormProperty)) {
				continue;
			}
			HistoricFormProperty fp = (HistoricFormProperty)detail;
			formData.put(fp.getPropertyId(), fp.getPropertyValue());
		}
	}
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskKey() {
		return taskKey;
	}

	public void setTaskKey(String taskKey) {
		this.taskKey = taskKey;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskAssignee() {
		return taskAssignee;
	}

	public void setTaskAssignee(String taskAssignee) {
		this.taskAssignee = taskAssignee;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Map<String, String> getFormData() {
		return formData;
	}

}
