package com.qfedu.common.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.IdentityLinkType;
import org.activiti.engine.task.Task;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qfedu.common.util.WoConstant;
import com.qfedu.common.util.WoUtil;

/**
 * @author cailei
 * @date Sep 11, 2018
 */
public class WoActivitiDto {

	/**
	 * 业务对象主键id
	 */
	private String businessKey;
	
	/**
	 * 流程实例id
	 */
	private String processInstanceId;
	
	/**
	 * 当前任务实例ID
	 */
	private String taskId;
	
	/**
	 * 当前任务定义ID,*.bpmn中的taskId
	 */
	private String taskKey;
	
	/**
	 * 当前任务名称
	 */
	private String taskName;

	/**
	 * activiti中的用户，对应当前用户的人员id
	 */
	private String currentUserId;
	
	private List<String> candidateUserIds = new ArrayList<String>();
	
	/**
	 * 开始时间
	 */
	private Date startTime;
	
	/**
	 * 结束时间
	 */
	private Date endTime;
	
	private List<WoActivitiTaskLog> taskLogs = new ArrayList<WoActivitiTaskLog>();
	
	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public String getTaskKey() {
		return taskKey;
	}

	public String getTaskName() {
		if (WoUtil.isEmpty(taskName)) {
			return "结束";
		}
		return taskName;
	}

	@JsonFormat(pattern = WoConstant.FORMAT_DATETIME, locale = "zh", timezone = "GMT+8")
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@JsonFormat(pattern = WoConstant.FORMAT_DATETIME, locale = "zh", timezone = "GMT+8")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	public String getCurrentUserId() {
		return currentUserId;
	}

	public void setCurrentUserId(String currentUserId) {
		this.currentUserId = currentUserId;
	}

	public List<WoActivitiTaskLog> getTaskLogs() {
		return taskLogs;
	}

	public void setTaskLogs(List<WoActivitiTaskLog> taskLogs) {
		this.taskLogs = taskLogs;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public void setTaskKey(String taskKey) {
		this.taskKey = taskKey;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/**
	 * @param task 当前任务
	 * @param users 当前任务的候选人id集合
	 * @param currentUserId 当前用户
	 */
	public void setTask (Task task, List<IdentityLink> users, String currentUserId) {
		if (task == null) {
			return;
		}
		this.taskId = task.getId();
		this.taskKey = task.getTaskDefinitionKey();
		this.taskName = task.getName();
		for (IdentityLink user : users) {
			if (IdentityLinkType.CANDIDATE.equals(user.getType())) {
				this.candidateUserIds.add(user.getUserId());
			}
		}
		this.currentUserId = currentUserId;
	}
	
	/**
	 * 如果当前用户是候选人，则可以处理该流程；否则，只能查看
	 * @return
	 */
	public Boolean hasProcessPermission () {
		return this.candidateUserIds.contains(this.currentUserId);
	}
	
	/**
	 * @return
	 */
	public String getOperationName () {
		return this.candidateUserIds.contains(this.currentUserId)? "处理" : "查看";
	}
}
