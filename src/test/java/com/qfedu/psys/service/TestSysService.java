package com.qfedu.psys.service;

import java.util.Arrays;
import java.util.Date;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.qfedu.common.util.WoUtil;
import com.qfedu.department.po.Position;
import com.qfedu.department.po.Staff;
import com.qfedu.department.repository.PositionRepository;
import com.qfedu.department.repository.StaffRepository;
import com.qfedu.psys.po.Dictionary;
import com.qfedu.psys.po.Menu;
import com.qfedu.psys.po.Role;
import com.qfedu.psys.po.User;
import com.qfedu.psys.repository.DictionaryRepository;
import com.qfedu.psys.repository.MenuRepository;
import com.qfedu.psys.repository.RoleRepository;
import com.qfedu.psys.repository.UserRepository;

/**
 * @author cailei
 * @date Nov 4, 2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSysService {
	
	private final static Logger LOG = LogManager.getLogger(TestSysService.class);

	@Resource
	private MenuRepository menuRepository;

	@Resource
	private DictionaryRepository dictionaryRepository;

	@Resource
	private RoleRepository roleRepository;

	@Resource
	private UserRepository userRepository;

	@Resource
	private PositionRepository positionRepository;
	
	@Resource
	private StaffRepository staffRepository;
	
	private void createPosition (String id, String name) {
		Position p = new Position ();
		p.setId(id);
		p.setName(name);
		positionRepository.save(p);
	}
	
	private Menu createMenu(String id, String icon, String name, String no, String url, Menu parent) {
		Menu m = new Menu();
		m.setId(id);
		m.setIcon(icon);
		m.setName(name);
		m.setNo(no);
		if (!WoUtil.isEmpty(url)) {
			m.setUrl(url);
		}
		m.setParent(parent);
		menuRepository.save(m);
		return m;
	}

	private void createDictionary(String id, String type, String value, String name, String desc, String params) {
		Dictionary d = new Dictionary();
		d.setId(id);
		d.setDicType(type);
		d.setVal(value);
		d.setName(name);
		d.setDescription(desc);
		d.setParams(params);
		dictionaryRepository.save(d);
	}

	@Test
	@Rollback(value = false)
	@Transactional
	public void save() {
		// 系统管理
		Menu m = createMenu("menu-psys", "icon-gears", "系统管理", "00", null, null);
		Menu user = createMenu("menu-user", "icon-male", "用户", "0010", "psys/user", m);
		Menu role = createMenu("menu-role", "icon-group", "角色", "0020", "psys/role", m);
		Menu menu = createMenu("menu-menu", "icon-bars", "菜单", "0030", "psys/menu", m);
		Menu dictionary = createMenu("menu-dictionary", "icon-file-text", "数据字典", "0040", "psys/dictionary", m);
		
		// 部门管理
		Menu dpt = createMenu("menu-department", "icon-gears", "部门管理", "10", null, null);
		Menu staff = createMenu("menu-staff", "icon-male", "人员", "1010", "department/staff", dpt);
		Menu department = createMenu("menu-department2", "icon-group", "部门", "1020", "department/department", dpt);
		Menu position = createMenu("menu-position", "icon-bars", "职务", "1030", "department/position", dpt);
		Menu myLeaves = createMenu("menu-myLeaves", "icon-bars", "我的请假", "1040", "department/leave/me", dpt);
		Menu allLeaves = createMenu("menu-allLeaves", "icon-bars", "所有请假", "1050", "department/leave/all", dpt);
		// 角色：系统管理员
		Role admin = new Role();
		admin.setId("admin");
		admin.setName("系统管理员");
		roleRepository.save(admin);
		// 角色：职员
		Role employee = new Role();
		employee.setId("employee");
		employee.setName("职员");
		employee.setMenus(Arrays.asList(dpt, myLeaves));
		roleRepository.save(employee);
		// 角色：管理者
		Role manager = new Role();
		manager.setId("manager");
		manager.setName("管理者");
		manager.setMenus(Arrays.asList(dpt, staff, department, position, myLeaves, allLeaves));
		roleRepository.save(manager);
		// 管理员用户
		User userAdmin = new User();
		userAdmin.setId("admin");
		userAdmin.setLoginName("admin");
		userAdmin.setPassword(WoUtil.getMD5(userAdmin.getId(), "cailei"));
		userAdmin.setCreateTime(new Date());
		userAdmin.setPasswordTime(new Date());
		userAdmin.setRoles(Arrays.asList(admin));
		userRepository.save(userAdmin);
		// 普通职员用户
		User userEmployee = new User();
		userEmployee.setId("employee");
		userEmployee.setLoginName("18900000000");
		userEmployee.setPassword(WoUtil.getMD5(userEmployee.getId(), "cailei"));
		userEmployee.setCreateTime(new Date());
		userEmployee.setPasswordTime(new Date());
		userEmployee.setRoles(Arrays.asList(employee));
		userRepository.save(userEmployee);
		Staff sfEmployee = new Staff ();
		sfEmployee.setId("employee");
		sfEmployee.setName("张三");
		sfEmployee.setMobile("18900000000");
		sfEmployee.setUser(userEmployee);
		staffRepository.save(sfEmployee);
		// 管理者
		User userManager = new User();
		userManager.setId("manager");
		userManager.setLoginName("18800000000");
		userManager.setPassword(WoUtil.getMD5(userManager.getId(), "cailei"));
		userManager.setCreateTime(new Date());
		userManager.setPasswordTime(new Date());
		userManager.setRoles(Arrays.asList(manager));
		userRepository.save(userManager);
		Staff sfManager = new Staff ();
		sfManager.setId("manager");
		sfManager.setName("李四");
		sfManager.setMobile("18800000000");
		sfManager.setUser(userManager);
		staffRepository.save(sfManager);
		
		createDictionary("1", "sex", "1", "男", "male", null);
		createDictionary("2", "sex", "2", "女", "female", null);
		
		this.createPosition("CEO", "CEO");
		this.createPosition("manager", "部门主管");
		this.createPosition("employee", "员工");
	}

}
